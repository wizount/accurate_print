﻿program CaseAssit;

uses
  Forms, windows,
  Main in 'Main.pas' {MainForm} ,
  GFunction in 'GFunction.pas',
  mprint in 'mprint.pas';

{$R *.res}

var
  Mutex: THandle;

begin

  Mutex := CreateMutex(nil, true, 'one'); { 第3个参数任意设置 }
  if GetLastError <> ERROR_ALREADY_EXISTS then
  begin
    Application.Initialize;
    Application.Title:='案件辅助软件';
    Application.CreateForm(TMainForm, MainForm);
    Application.Run;
  end
  else
    Application.MessageBox('该程序正在运行！', '提示', MB_OK);
  ReleaseMutex(Mutex); { 释放资源 }

end.

