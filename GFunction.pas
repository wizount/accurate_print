unit GFunction;

interface

uses
  Windows, SysUtils, Classes;

  function bgr2rgb(pbySrc:pByte; iSrcSize:Integer; pbyDst:pByte; piDstSize:pInteger; iWidth:Integer; iHeight:Integer) : Integer;
  function tool_WriteOneFile(strFileName:String; pbyBuf:pByte; iBufSize:Integer) : Integer;
  function tool_ReadOneFile(strFileName:String; pbyBuf:pByte; iBufSize:Integer) : Integer;

implementation

{
功能：
    对BGR格式数据进行B、R转换，只支持24位深度图像。
参数：
    [IN]pbySrc
        bgr数据。
    [IN]iSrcSize
        bgr数据大小。
    [OUT]pbyDst
        返回转换后的rgb格式数据，需要开((pi_iWidth * 3 + 3) / 4) * 4 * pi_iHeight字节空间。
    [IN,OUT]iDstSize
        传入开辟空间大小，函数执行成功后返回转换后的rgb格式数据大小。
    [IN]iWidth
        图片宽度（像素）。
    [IN]iHeight
        图片高度（像素）。
返回值：
	0 执行成功。
}
function bgr2rgb(pbySrc:pByte; iSrcSize:Integer; pbyDst:pByte; piDstSize:pInteger; iWidth:Integer; iHeight:Integer) : Integer;
var
  iWidthSize, iDstWidthSize, iExternSize : Integer;
  iPosX, iPosY : Integer;
begin
  iWidthSize := iWidth * 3;
  iDstWidthSize := ((iWidth * 3 + 3) div 4) * 4;
  iExternSize := ((iWidth * 3 + 3) div 4) * 4 - iWidth * 3;

  if iSrcSize <> (iWidthSize * iHeight) then
  begin
    result := -1;
    exit;
  end;

  if piDstSize^ < (iDstWidthSize * iHeight) then
  begin
    result := -2;
    exit;
  end;

  FillMemory(pbyDst, piDstSize^, 0);

  for iPosY := 0 to iHeight - 1 do
  begin
    for iPosX := 0 to iWidth*3 - 1 do
    begin
      if (iPosX mod 3 = 0) then
      begin
        pbyDst[(iWidthSize + iExternSize) * iPosY + iPosX + 0] := pbySrc[iWidthSize * iPosY + iPosX + 2];
        pbyDst[(iWidthSize + iExternSize) * iPosY + iPosX + 1] := pbySrc[iWidthSize * iPosY + iPosX + 1];
        pbyDst[(iWidthSize + iExternSize) * iPosY + iPosX + 2] := pbySrc[iWidthSize * iPosY + iPosX + 0];
      end;
    end;
  end;

  piDstSize^ := iDstWidthSize * iHeight;

  result := 0;
end;


function tool_WriteOneFile(strFileName:String; pbyBuf:pByte; iBufSize:Integer) : Integer;
var
  iWriteNum : Integer;
  fileStream : TFileStream;
begin
  iWriteNum := 0;

  if pbyBuf = nil then
  begin
    result := iWriteNum;
    exit;
  end;

  fileStream := TFileStream.Create(strFileName, fmCreate);
  fileStream.Position := 0;

  iWriteNum := fileStream.Write(pbyBuf[0], iBufSize);

  fileStream.Free;

  result := iWriteNum;
end;

function tool_ReadOneFile(strFileName:String; pbyBuf:pByte; iBufSize:Integer) : Integer;
var
  iWriteNum : Integer;
  fileStream : TFileStream;
begin
  iWriteNum := 0;

  if pbyBuf = nil then
  begin
    result := iWriteNum;
    exit;
  end;

  fileStream := TFileStream.Create(strFileName, fmOpenRead);

  fileStream.Position := 0;

  fileStream.Read(pbyBuf^, iBufSize);

  result := iWriteNum;
end;

end.
