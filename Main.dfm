object MainForm: TMainForm
  Left = 0
  Top = 0
  Caption = #26696#20214#36741#21161#36719#20214
  ClientHeight = 403
  ClientWidth = 793
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 472
    Top = 314
    Width = 313
    Height = 81
    AutoSize = False
    Caption = #27492#36719#20214#20026#26696#20214#31649#29702#31995#32479#36741#21161#36719#20214#12290#38656#35201#35835#21462#36523#20221#35777#21644#25991#20070#22871#25171#65292#35831#25171#24320#12290
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object GroupBox_IdCard: TGroupBox
    Left = 8
    Top = 21
    Width = 425
    Height = 374
    Caption = #20108#20195#36523#20221#35777#35835#21462
    TabOrder = 0
    object Image_Photo: TImage
      Left = 303
      Top = 90
      Width = 96
      Height = 129
    end
    object Label1: TLabel
      Left = 21
      Top = 21
      Width = 144
      Height = 13
      Caption = #35831#36873#25321#36523#20221#35777#38405#35835#22120#31471#21475#65306
    end
    object Label2: TLabel
      Left = 24
      Top = 40
      Width = 48
      Height = 13
      Caption = #27874#29305#29575#65306
    end
    object Button_Read: TButton
      Left = 303
      Top = 241
      Width = 75
      Height = 25
      Caption = #35835#21345
      TabOrder = 0
      OnClick = Button_ReadClick
    end
    object Button_Clear: TButton
      Left = 303
      Top = 280
      Width = 75
      Height = 25
      Caption = #28165#31354
      TabOrder = 1
      OnClick = Button_ClearClick
    end
    object memo_msg: TMemo
      Left = 21
      Top = 67
      Width = 252
      Height = 294
      Lines.Strings = (
        'Memo_Msg')
      ScrollBars = ssVertical
      TabOrder = 2
    end
    object ComboBox_Baud: TComboBox
      Left = 171
      Top = 40
      Width = 124
      Height = 21
      Style = csDropDownList
      TabOrder = 3
    end
    object ComboBox_Port: TComboBox
      Left = 171
      Top = 13
      Width = 115
      Height = 21
      Style = csDropDownList
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 472
    Top = 21
    Width = 313
    Height = 129
    Caption = #38024#24335#25171#21360#26426#35774#32622
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 24
      Top = 33
      Width = 72
      Height = 13
      Caption = #36873#25321#25171#21360#26426#65306
    end
    object Label4: TLabel
      Left = 24
      Top = 65
      Width = 48
      Height = 13
      Caption = #27178#20559#31227#65306
    end
    object Label5: TLabel
      Left = 24
      Top = 97
      Width = 48
      Height = 13
      Caption = #31446#20559#31227#65306
    end
    object LinkLabel2: TLinkLabel
      Left = 151
      Top = 63
      Width = 40
      Height = 17
      Caption = '<a>'#23383#38752#24038'</a>'
      TabOrder = 0
      OnClick = LinkLabel2Click
    end
    object LinkLabel3: TLinkLabel
      Left = 210
      Top = 63
      Width = 40
      Height = 17
      Caption = '<a>'#23383#38752#21491'</a>'
      TabOrder = 1
      OnClick = LinkLabel3Click
    end
    object LinkLabel4: TLinkLabel
      Left = 150
      Top = 91
      Width = 40
      Height = 17
      Caption = '<a>'#23383#38752#19978'</a>'
      TabOrder = 2
      OnClick = LinkLabel4Click
    end
    object LinkLabel5: TLinkLabel
      Left = 209
      Top = 91
      Width = 40
      Height = 17
      Caption = '<a>'#23383#38752#19979'</a>'
      TabOrder = 3
      OnClick = LinkLabel5Click
    end
    object offXSE: TSpinEdit
      Left = 90
      Top = 62
      Width = 47
      Height = 22
      Hint = #36127#25968#25991#23383#24448#24038#20559#31227#65292#27491#25968#24448#21491#20559#31227
      MaxValue = 0
      MinValue = 0
      TabOrder = 4
      Value = 0
      OnChange = offXSEChange
    end
    object offYSE: TSpinEdit
      Left = 90
      Top = 90
      Width = 47
      Height = 22
      Hint = #36127#25968#25991#23383#24448#19978#20559#31227#65292#27491#25968#24448#19979#20559#31227
      MaxValue = 0
      MinValue = 0
      TabOrder = 5
      Value = 0
      OnChange = offYSEChange
    end
    object printerCom: TComboBox
      Left = 102
      Top = 30
      Width = 160
      Height = 22
      Hint = #36873#25321#19968#20010#40664#35748#25171#21360#26426
      Style = csOwnerDrawFixed
      TabOrder = 6
      OnChange = printerComChange
    end
  end
  object IdHTTPServer1: TIdHTTPServer
    Active = True
    Bindings = <>
    DefaultPort = 9294
    OnCommandGet = IdHTTPServer1CommandGet
    Left = 344
    Top = 32
  end
end
